package com.main;

import java.awt.*;
import java.awt.event.*;

public class InputHandler implements MouseListener, MouseMotionListener, KeyListener {

    private boolean[] keys = new boolean[65536];
    private boolean[] keysPressed = new boolean[65536];

    private boolean[] buttons = new boolean[16];
    private boolean[] buttonsPressed = new boolean[16];

    private Point mouse = new Point();

    public boolean isKeyDown(int keyCode) {
        return keys[keyCode];
    }

    boolean isButtonDown(int buttonCode) {
        return buttons[buttonCode];
    }

    public boolean isKeyPressed(int keyCode) {
        if (keys[keyCode] && !keysPressed[keyCode]) {
            keysPressed[keyCode] = true;
            return true;
        }
        return false;
    }

    public boolean isButtonPressed(int buttonCode) {
        if (buttons[buttonCode] && !buttonsPressed[buttonCode]) {
            buttonsPressed[buttonCode] = true;
            return true;
        }
        return false;
    }

    public double getMouseX() {
        return mouse.getX();
    }

    public double getMouseY() {
        return mouse.getY();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
        keysPressed[e.getKeyCode()] = false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        buttons[e.getButton()] = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        buttons[e.getButton()] = false;
        buttonsPressed[e.getButton()] = false;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouse.setLocation(e.getX(), e.getY());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouse.setLocation(e.getX(), e.getY());
    }
}

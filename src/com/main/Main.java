package com.main;

import com.rendering.Display;

public class Main implements Runnable {

    private static final int PIXEL_SIZE = 1;
    private static final int WIDTH = 800 / PIXEL_SIZE;
    private static final int HEIGHT = 600 / PIXEL_SIZE;

    private Application application;
    private Display display;

    private boolean running = false;

    private Main() {
        display = new Display(WIDTH, HEIGHT, PIXEL_SIZE);
        application = new Application(display);

        Thread thread = new Thread(this);

        running = true;
        thread.start();
    }

    public static void main(String[] args) {
        new Main();
    }

    @Override
    public void run() {

        long previousTick = System.nanoTime();
        long timer = System.currentTimeMillis();
        int frames = 0;

        while (running) {

            long currentTick = System.nanoTime();
            float delta = (currentTick - previousTick) / (1000000000.0f / 60.0f);
            previousTick = currentTick;

            application.update(delta);

            display.clearZBuffer();
            display.getBuffer().clear((byte) 0, (byte) 0, (byte) 0);
            application.render();

            display.swapBuffers();

            frames++;
            if (System.currentTimeMillis() >= timer + 1000) {
                System.out.println("FPS: " + frames);
                frames = 0;
                timer += 1000;
            }

            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

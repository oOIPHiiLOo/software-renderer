package com.main;

import com.engine.Camera;
import com.rendering.Display;
import com.rendering.TexturedModelRenderer;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

class Application {

    private Display display;
    private InputHandler input;
    private TexturedModelRenderer modelRenderer;
    private Camera camera;

    Application(Display display) {
        this.display = display;
        input = display.getInputHandler();
        modelRenderer = new TexturedModelRenderer(display);
        camera = new Camera().addInputHandler(input).addDisplay(display);
        modelRenderer.setCamera(camera);
    }

    void update(float delta) {
        display.setNear(0.01f);
        display.setFar(100);
        camera.update(delta);
        if (input.isButtonDown(MouseEvent.BUTTON1) && !display.isMouseCaptured())
            display.captureMouse();
        if (input.isKeyDown(KeyEvent.VK_ESCAPE) && display.isMouseCaptured())
            display.uncaptureMouse();
    }

    void render() {

    }
}

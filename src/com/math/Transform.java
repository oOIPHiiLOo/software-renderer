package com.math;

public class Transform {

    private Matrix4f transformationMatrix;

    public Transform() {
        clear();
    }

    public void clear() {
        if (transformationMatrix == null)
            transformationMatrix = new Matrix4f();
        transformationMatrix.initIdentity();
    }

    public Transform translate(float x, float y, float z) {
        transformationMatrix = new Matrix4f().initTranslation(x, y, z).mul(transformationMatrix);
        return this;
    }

    public Transform rotateX(float x) {
        Matrix4f xRotation = new Matrix4f().initRotationX(x);
        transformationMatrix = xRotation.mul(transformationMatrix);
        return this;
    }

    public Transform rotateY(float y) {
        Matrix4f yRotation = new Matrix4f().initRotationY(y);
        transformationMatrix = yRotation.mul(transformationMatrix);
        return this;
    }


    public Transform rotateZ(float z) {
        Matrix4f zRotation = new Matrix4f().initRotationZ(z);
        transformationMatrix = zRotation.mul(transformationMatrix);
        return this;
    }

    public Transform scale(float x, float y, float z) {
        Matrix4f scale = new Matrix4f().initScale(x, y, z);
        transformationMatrix = scale.mul(transformationMatrix);
        return this;
    }

    public Transform toScreenSpace(int halfWidth, int halfHeight) {
        transformationMatrix = new Matrix4f().initScreenSpaceMatrix(halfWidth, halfHeight);
        return this;
    }

    void apply(Vector3f vec) {
        Matrix4f m = new Matrix4f().initIdentity();
        m.set(0, 3, vec.getX());
        m.set(1, 3, vec.getY());
        m.set(2, 3, vec.getZ());

        Matrix4f applied = transformationMatrix.mul(m);

        vec.setX(applied.get(0, 3));
        vec.setY(applied.get(1, 3));
        vec.setZ(applied.get(2, 3));
    }

    public Transform apply(Transform t) {
        transformationMatrix = t.getMatrix().mul(transformationMatrix);
        return this;
    }

    Matrix4f getMatrix() {
        return transformationMatrix;
    }

    public void apply(Vector3f[] positions) {
        for (Vector3f pos : positions) {
            apply(pos);
        }
    }
}

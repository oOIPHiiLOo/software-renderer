package com.math;

public class Vector3f {

    private float x;
    private float y;
    private float z;

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void perspectiveDivide() {
        x /= z;
        y /= z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public Vector3f cross(Vector3f vec) {
        return new Vector3f(
                this.y * vec.getZ() - this.z * vec.getY(),
                this.z * vec.getX() - this.x * vec.getZ(),
                this.x * vec.getY() - this.y * vec.getX()
        );
    }

    public Vector3f sub(Vector3f vec) {
        return new Vector3f(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ());
    }

    public Vector3f add(Vector3f vec) {
        return new Vector3f(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
    }

    public Vector3f copy() {
        return new Vector3f(x, y, z);
    }

    public void set(Vector3f v) {
        x = v.getX();
        y = v.getY();
        z = v.getZ();
    }
}

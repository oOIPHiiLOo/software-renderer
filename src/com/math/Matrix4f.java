package com.math;

class Matrix4f {

    private float[][] m;

    Matrix4f() {
        m = new float[4][4];
    }

    Matrix4f initIdentity() {
        m[0][0] = 1;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = 0;
        m[1][0] = 0;
        m[1][1] = 1;
        m[1][2] = 0;
        m[1][3] = 0;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = 1;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
        return this;
    }

    Matrix4f initTranslation(float x, float y, float z) {
        m[0][0] = 1;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = x;
        m[1][0] = 0;
        m[1][1] = 1;
        m[1][2] = 0;
        m[1][3] = y;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = 1;
        m[2][3] = z;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
        return this;
    }

    Matrix4f initScale(float x, float y, float z) {
        m[0][0] = x;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = 0;
        m[1][0] = 0;
        m[1][1] = y;
        m[1][2] = 0;
        m[1][3] = 0;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = z;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
        return this;
    }

    Matrix4f initRotationZ(float z) {

        z = (float) Math.toRadians(z);

        float cosz = (float) Math.cos(z);
        float sinz = (float) Math.sin(z);

        m[0][0] = cosz;
        m[0][1] = -sinz;
        m[0][2] = 0;
        m[0][3] = 0;
        m[1][0] = sinz;
        m[1][1] = cosz;
        m[1][2] = 0;
        m[1][3] = 0;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = 1;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;

        return this;
    }

    Matrix4f initRotationX(float x) {

        x = (float) Math.toRadians(x);

        float cosx = (float) Math.cos(x);
        float sinx = (float) Math.sin(x);

        m[0][0] = 1;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = 0;
        m[1][0] = 0;
        m[1][1] = cosx;
        m[1][2] = sinx;
        m[1][3] = 0;
        m[2][0] = 0;
        m[2][1] = -sinx;
        m[2][2] = cosx;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
        return this;
    }

    Matrix4f initRotationY(float y) {

        y = (float) Math.toRadians(y);

        float cosy = (float) Math.cos(y);
        float siny = (float) Math.sin(y);

        m[0][0] = cosy;
        m[0][1] = 0;
        m[0][2] = -siny;
        m[0][3] = 0;
        m[1][0] = 0;
        m[1][1] = 1;
        m[1][2] = 0;
        m[1][3] = 0;
        m[2][0] = siny;
        m[2][1] = 0;
        m[2][2] = cosy;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;
        return this;
    }

    Matrix4f initScreenSpaceMatrix(int halfWidth, int halfHeight) {
        m[0][0] = halfHeight;
        m[0][1] = 0;
        m[0][2] = 0;
        m[0][3] = halfWidth;
        m[1][0] = 0;
        m[1][1] = -halfHeight;
        m[1][2] = 0;
        m[1][3] = halfHeight;
        m[2][0] = 0;
        m[2][1] = 0;
        m[2][2] = 1;
        m[2][3] = 0;
        m[3][0] = 0;
        m[3][1] = 0;
        m[3][2] = 0;
        m[3][3] = 1;

        return this;
    }

    Matrix4f mul(Matrix4f b) {
        Matrix4f ret = new Matrix4f();

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                ret.set(i, j, m[i][0] * b.get(0, j) +
                        m[i][1] * b.get(1, j) +
                        m[i][2] * b.get(2, j) +
                        m[i][3] * b.get(3, j));
            }
        }

        return ret;
    }

    float get(int row, int col) {
        return m[row][col];
    }

    void set(int row, int col, float val) {
        m[row][col] = val;
    }
}

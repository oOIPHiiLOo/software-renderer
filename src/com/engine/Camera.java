package com.engine;

import com.main.InputHandler;
import com.rendering.Display;

import java.awt.event.KeyEvent;

public class Camera {

    private float x, y, z;
    private float velx, vely, velz;

    private float rx, ry;

    private InputHandler input;
    private Display display;

    public void update(float delta) {
        float speed = 0.05f;

        float rry = (float) Math.toRadians(ry);
        boolean moving = false;
        if (input.isKeyDown(KeyEvent.VK_W)) {
            velx += Math.sin(rry) * speed * delta;
            velz += Math.cos(rry) * speed * delta;
            moving = true;
        }
        if (input.isKeyDown(KeyEvent.VK_A)) {
            velx += Math.sin(rry - Math.toRadians(90)) * speed * delta;
            velz += Math.cos(rry - Math.toRadians(90)) * speed * delta;
            moving = true;
        }
        if (input.isKeyDown(KeyEvent.VK_D)) {
            velx += Math.sin(rry + Math.toRadians(90)) * speed * delta;
            velz += Math.cos(rry + Math.toRadians(90)) * speed * delta;
            moving = true;
        }
        if (input.isKeyDown(KeyEvent.VK_S)) {
            velx += Math.sin(rry + Math.toRadians(180)) * speed * delta;
            velz += Math.cos(rry + Math.toRadians(180)) * speed * delta;
            moving = true;
        }
        if (input.isKeyDown(KeyEvent.VK_SPACE)) {
            vely += speed * delta;
            moving = true;
        }
        if (input.isKeyDown(KeyEvent.VK_SHIFT)) {
            vely -= speed * delta;
            moving = true;
        }

        if (display.isMouseCaptured()) {
            display.moveMouseToCentre();

            float mouseDx = (float) (input.getMouseX() - display.getWidth() / 2);
            float mouseDy = (float) (input.getMouseY() - display.getHeight() / 2);

            rx += mouseDy / 5;
            ry += mouseDx / 5;
        }

        x += velx * delta;
        y += vely * delta;
        z += velz * delta;

        if (!moving) {
            velx *= 1 - (0.1 * delta);
            vely *= 1 - (0.1 * delta);
            velz *= 1 - (0.1 * delta);
        }
    }

    public Camera addInputHandler(InputHandler input) {
        this.input = input;
        return this;
    }

    public void setPos(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getRx() {
        return rx;
    }

    public void setRx(float rx) {
        this.rx = rx;
    }

    public float getRy() {
        return ry;
    }

    public void setRy(float ry) {
        this.ry = ry;
    }

    public Camera addDisplay(Display display) {
        this.display = display;
        return this;
    }
}

package com.rendering;

import com.main.InputHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class Display extends Canvas {

    private int width;
    private int height;
    private int pixelSize;

    private InputHandler inputHandler;
    private JFrame frame;

    private BufferedImage screen;
    private byte[] screenRaster;

    private Bitmap buffer;
    private float[] zBuffer;
    private float far;
    private float near;

    private boolean mouseCaptured = true;

    public Display(int width, int height, int pixelSize) {
        this.width = width;
        this.height = height;
        this.pixelSize = pixelSize;

        Dimension size = new Dimension(width * pixelSize, height * pixelSize);

        setMinimumSize(size);
        setPreferredSize(size);
        setMaximumSize(size);

        frame = new JFrame();

        frame.setResizable(false);
        frame.add(this);
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setTitle("Archer");
        frame.setVisible(true);
        frame.requestFocus();
        requestFocus();

        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg, new Point(0, 0), "blank cursor");
        frame.getContentPane().setCursor(blankCursor);

        inputHandler = new InputHandler();

        addMouseListener(inputHandler);
        addMouseMotionListener(inputHandler);
        addKeyListener(inputHandler);

        screen = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        screenRaster = ((DataBufferByte) screen.getRaster().getDataBuffer()).getData();

        buffer = new Bitmap(width, height);
        zBuffer = new float[width * height];

        moveMouseToCentre();
    }

    public void swapBuffers() {
        BufferStrategy bufferStrategy = getBufferStrategy();
        Graphics graphics = bufferStrategy.getDrawGraphics();

        buffer.copyToByteArray(screenRaster);
        graphics.drawImage(screen, 0, 0, width * pixelSize, height * pixelSize, null);

        graphics.dispose();
        bufferStrategy.show();
    }

    public Bitmap getBuffer() {
        return buffer;
    }

    void setZBuffer(int x, int y, float val) {
        int index = x + y * width;
        zBuffer[index] = val;
    }

    float getZBuffer(int x, int y) {
        int index = x + y * width;
        return zBuffer[index];
    }

    public void clearZBuffer() {
        for (int i = 0; i < zBuffer.length; i++) {
            zBuffer[i] = far;
        }
    }

    @Override
    public BufferStrategy getBufferStrategy() {
        if (super.getBufferStrategy() == null) {
            createBufferStrategy(1);
        }
        return super.getBufferStrategy();
    }


    public InputHandler getInputHandler() {
        return inputHandler;
    }

    float getFar() {
        return far;
    }

    public void moveMouseToCentre() {
        try {
            Robot robot = new Robot();
            robot.mouseMove(
                    (int) (getWidth() / 2 + frame.getLocationOnScreen().getX() + frame.getInsets().left),
                    (int) (getHeight() / 2 + frame.getLocationOnScreen().getY() + frame.getInsets().top));
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public void captureMouse() {
        mouseCaptured = true;
        moveMouseToCentre();
    }

    public void uncaptureMouse() {
        mouseCaptured = false;
    }

    public boolean isMouseCaptured() {
        return mouseCaptured;
    }

    public float getNear() {
        return near;
    }

    public void setNear(float near) {
        this.near = near;
    }

    public void setFar(int far) {
        this.far = far;
    }
}

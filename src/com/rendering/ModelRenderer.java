package com.rendering;

import com.engine.Camera;
import com.math.Transform;
import com.math.Vector3f;
import com.model.Face;
import com.model.Model;
import com.model.Vertex;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ModelRenderer {

    Display display;
    Bitmap buffer;

    int width;
    int height;

    float[][] scanBuffer;
    Model currentModel;
    private Camera camera = null;
    private byte[] pixelColour = new byte[4];

    ModelRenderer(Display display) {
        this.display = display;
        buffer = display.getBuffer();
        width = display.getBuffer().getWidth();
        height = display.getBuffer().getHeight();
        scanBuffer = new float[height * 2][4];
    }

    public void renderModel(Model model) {
        currentModel = model;

        Vector3f[] positions = copy(model.getPositions());
        Vector3f[] textureCoordinates = model.getTextureCoordinates();

        Face[] faces = model.getFaces();
        ArrayList<Vertex[]> triangles = new ArrayList<>();

        model.getTransform().apply(new Transform()
                .translate(-camera.getX(), -camera.getY(), -camera.getZ())
                .rotateY(camera.getRy())
                .rotateX(camera.getRx())
        ).apply(positions);

        for (Face face : faces) {
            triangles.add(constructVertexArrayFromFace(face, positions, textureCoordinates));
        }

        farClip(triangles);

        positions = concatenate(positions, nearClip(triangles));

        perspectiveDivide(positions);

        backfaceCull(triangles);

        new Transform().toScreenSpace(width / 2, height / 2).apply(positions);

        renderTriangles(triangles.toArray(new Vertex[triangles.size()][3]));

        model.getTransform().clear();
    }

    private <T> T[] concatenate(T[] a, T[] b) {
        int aLen = a.length;
        int bLen = b.length;

        @SuppressWarnings("unchecked")
        T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);

        return c;
    }

    private void backfaceCull(ArrayList<Vertex[]> triangles) {
        Iterator<Vertex[]> i = triangles.iterator();
        while (i.hasNext()) {
            Vertex[] v = i.next();
            if (v[1].getPosition().sub(v[0].getPosition()).cross(v[2].getPosition().sub(v[0].getPosition())).getZ() > 0)
                i.remove();
        }
    }

    private void farClip(ArrayList<Vertex[]> triangles) {
        for (int i = 0; i < triangles.size(); i++) {
            Vertex[] vertices = triangles.get(i);
            int count = 0;
            for (Vertex vertex : vertices) {
                if (vertex.getPosition().getZ() >= display.getFar())
                    count++;
            }
            if (count == 3) {
                triangles.remove(i);
                i--;
            }
        }
    }

    private Vector3f[] nearClip(ArrayList<Vertex[]> triangles) {
        ArrayList<Vector3f> newPositions = new ArrayList<>();
        float near = display.getNear();
        for (int i = 0; i < triangles.size(); i++) {
            Vertex[] vertices = triangles.get(i);
            int count = getVertexCountBeyondNear(vertices, near);
            switch (count) {
                case 0:
                    continue;
                case 1:
                    newPositions.addAll(Arrays.asList(cutTriangleWithOne(near, vertices, triangles)));
                    break;
                case 2:
                    newPositions.addAll(Arrays.asList(cutTriangleWithTwo(near, vertices)));
                    break;
                case 3:
                    triangles.remove(i);
                    i--;
                    break;
            }
        }
        return newPositions.toArray(new Vector3f[newPositions.size()]);
    }

    private Vector3f[] cutTriangleWithTwo(float near, Vertex[] vertices) {
        ArrayList<Vector3f> positions = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            int toIndex = i == 2 ? 0 : i + 1;

            if (vertices[i].getPosition().getZ() < near && vertices[toIndex].getPosition().getZ() > near) {
                vertices[i] = getIntersection(vertices[i], vertices[toIndex], near);
                positions.add(vertices[i].getPosition());
            }

            if (vertices[i].getPosition().getZ() > near && vertices[toIndex].getPosition().getZ() < near) {
                vertices[toIndex] = getIntersection(vertices[i], vertices[toIndex], near);
                positions.add(vertices[toIndex].getPosition());
            }
        }
        return positions.toArray(new Vector3f[positions.size()]);
    }

    private Vector3f[] cutTriangleWithOne(float near, Vertex[] vertices, ArrayList<Vertex[]> triangles) {
        Vertex a = null;
        Vertex b = null;
        Vertex c = null;
        Vertex d = null;

        for (int i = 0; i < 3; i++) {
            int toIndex = i == 2 ? 0 : i + 1;

            if (vertices[i].getPosition().getZ() < near && vertices[toIndex].getPosition().getZ() >= near) {
                d = getIntersection(vertices[i], vertices[toIndex], near);
            } else if (vertices[i].getPosition().getZ() >= near && vertices[toIndex].getPosition().getZ() < near) {
                c = getIntersection(vertices[i], vertices[toIndex], near);
            } else {
                a = vertices[i];
                b = vertices[toIndex];
            }
        }

        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;

        triangles.add(new Vertex[]{a, c, d});

        return new Vector3f[]{c.getPosition(), d.getPosition()};
    }

    private int getVertexCountBeyondNear(Vertex[] vertices, float near) {
        int count = 0;
        for (Vertex vertex : vertices) {
            if (vertex.getPosition().getZ() < near)
                count++;
        }
        return count;
    }

    protected Vertex getIntersection(Vertex a, Vertex b, float near) {
        float ax = a.getPosition().getX();
        float ay = a.getPosition().getY();
        float az = a.getPosition().getZ();
        float atx = a.getTextureCoordinate().getX();
        float aty = a.getTextureCoordinate().getY();

        float bx = b.getPosition().getX();
        float by = b.getPosition().getY();
        float bz = b.getPosition().getZ();
        float btx = b.getTextureCoordinate().getX();
        float bty = b.getTextureCoordinate().getY();

        float dx = bx - ax;
        float dy = by - ay;
        float dtx = btx - atx;
        float dty = bty - aty;

        float change = (near - az) / (bz - az);

        return new Vertex(
                new Vector3f(
                        ax + dx * change,
                        ay + dy * change,
                        near
                ),
                new Vector3f(
                        atx + dtx * change,
                        aty + dty * change,
                        0
                )
        );
    }

    private void renderTriangles(Vertex[][] triangles) {
        for (Vertex[] vertices : triangles) {
            renderTriangle(vertices);
        }
    }

    private void renderTriangle(Vertex[] vertices) {
        orderVertices(vertices);

        Vector3f min = vertices[0].getPosition();
        Vector3f mid = vertices[1].getPosition();
        Vector3f max = vertices[2].getPosition();

        int handedness = mid.sub(min).cross(max.sub(min)).getZ() > 0 ? 1 : 0;

        scanLineToBuffer(vertices[0], vertices[2], 1 - handedness);
        scanLineToBuffer(vertices[0], vertices[1], handedness);
        scanLineToBuffer(vertices[1], vertices[2], handedness);

        int minRender = (int) Math.ceil(min.getY()) > height ? height :
                (int) Math.ceil(min.getY()) > 0 ? (int) Math.ceil(min.getY()) : 0;

        int maxRender = (int) Math.ceil(max.getY());

        if (maxRender >= height)
            maxRender = height;

        renderScanBuffer(minRender, maxRender);
    }

    protected void renderScanBuffer(int min, int max) {
        for (int y = min; y < max; y++) {
            int minx = (int) Math.ceil(scanBuffer[y * 2][0]);
            int maxx = (int) Math.ceil(scanBuffer[y * 2 + 1][0]);

            float deltay = 1 / (scanBuffer[y * 2 + 1][0] - scanBuffer[y * 2][0]);
            float overflow = minx - scanBuffer[y * 2][0];

            float zStep = (scanBuffer[y * 2 + 1][1] - scanBuffer[y * 2][1]) * deltay;
            float z = scanBuffer[y * 2][1] + zStep * overflow;

            float txStep = (scanBuffer[y * 2 + 1][2] - scanBuffer[y * 2][2]) * deltay;
            float tx = scanBuffer[y * 2][2] + txStep * overflow;

            float tyStep = (scanBuffer[y * 2 + 1][3] - scanBuffer[y * 2][3]) * deltay;
            float ty = scanBuffer[y * 2][3] + tyStep * overflow;

            if (minx < 0) {
                z += zStep * -minx;
                tx += txStep * -minx;
                ty += tyStep * -minx;
                minx = 0;
            }

            if (maxx > width)
                maxx = width;

            for (int x = minx; x < maxx; x++) {
                float oneOverZ = 1 / z;
                if (display.getZBuffer(x, y) < oneOverZ) {
                    z += zStep;
                    tx += txStep;
                    ty += tyStep;
                    continue;
                }
                display.setZBuffer(x, y, oneOverZ);

                currentModel.getPixelAt(tx * oneOverZ, ty * oneOverZ, pixelColour);
                float lightness = (display.getFar() - oneOverZ) / display.getFar();
                byte r = (byte) ((pixelColour[1] & 0xff) * lightness);
                byte g = (byte) ((pixelColour[2] & 0xff) * lightness);
                byte b = (byte) ((pixelColour[3] & 0xff) * lightness);

                buffer.set(x, y, r, g, b);

                z += zStep;
                tx += txStep;
                ty += tyStep;
            }
        }
    }

    protected void scanLineToBuffer(Vertex av, Vertex bv, int handedness) {
        Vector3f a = av.getPosition();
        Vector3f b = bv.getPosition();

        Vector3f at = av.getTextureCoordinate();
        Vector3f bt = bv.getTextureCoordinate();

        int miny = (int) Math.ceil(a.getY());
        int maxy = (int) Math.ceil(b.getY());

        float deltay = 1 / (b.getY() - a.getY());
        float overflow = miny - a.getY();

        float oneOverAZ = 1 / a.getZ();
        float oneOverBZ = 1 / b.getZ();

        float xStep = (b.getX() - a.getX()) * deltay;
        float x = a.getX() + xStep * overflow;

        float zStep = (oneOverBZ - oneOverAZ) * deltay;
        float z = oneOverAZ + zStep * overflow;

        float txStep = (bt.getX() * oneOverBZ - at.getX() * oneOverAZ) * deltay;
        float tx = (at.getX() * oneOverAZ) + txStep * overflow;

        float tyStep = (bt.getY() * oneOverBZ - at.getY() * oneOverAZ) * deltay;
        float ty = (at.getY() * oneOverAZ) + tyStep * overflow;

        if (miny < 0) {
            x += xStep * -miny;
            z += zStep * -miny;
            tx += txStep * -miny;
            ty += tyStep * -miny;
            miny = 0;
        }
        if (maxy > height)
            maxy = height;

        for (int y = miny; y < maxy; y++) {
            scanBuffer[y * 2 + handedness][0] = x;
            scanBuffer[y * 2 + handedness][1] = z;
            scanBuffer[y * 2 + handedness][2] = tx;
            scanBuffer[y * 2 + handedness][3] = ty;

            x += xStep;
            z += zStep;
            tx += txStep;
            ty += tyStep;
        }
    }

    private void orderVertices(Vertex[] vertices) {
        if (vertices[1].getPosition().getY() > vertices[2].getPosition().getY()) {
            Vertex temp = vertices[2];
            vertices[2] = vertices[1];
            vertices[1] = temp;
        }
        if (vertices[0].getPosition().getY() > vertices[1].getPosition().getY()) {
            Vertex temp = vertices[1];
            vertices[1] = vertices[0];
            vertices[0] = temp;
        }
        if (vertices[1].getPosition().getY() > vertices[2].getPosition().getY()) {
            Vertex temp = vertices[2];
            vertices[2] = vertices[1];
            vertices[1] = temp;
        }
    }

    private void perspectiveDivide(Vector3f[] vertices) {
        for (Vector3f vertex : vertices) {
            vertex.perspectiveDivide();
        }
    }

    private Vector3f[] copy(Vector3f[] v) {
        if (v == null)
            return null;
        Vector3f[] copiedVector = new Vector3f[v.length];
        for (int i = 0; i < v.length; i++) {
            copiedVector[i] = v[i].copy();
        }
        return copiedVector;
    }

    private Vertex[] constructVertexArrayFromFace(Face face, Vector3f[] positions, Vector3f[] textureCoordinates) {
        Vertex[] vertices = new Vertex[3];
        for (int i = 0; i < 3; i++) {
            Vector3f p = null;
            Vector3f t = null;

            if (positions != null)
                p = positions[face.getPositions()[i]];
            if (textureCoordinates != null)
                t = textureCoordinates[face.getTextureCoordinates()[i]];

            vertices[i] = new Vertex(p, t);
        }
        return vertices;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }
}
package com.rendering;

public class Bitmap {

    private int width;
    private int height;

    private Byte[] raster;

    Bitmap(int width, int height) {
        this.width = width;
        this.height = height;
        raster = new Byte[width * height * 3];
    }

    void copyToByteArray(byte[] dest) {
        if (dest.length != raster.length)
            return;
        for (int i = 0; i < raster.length; i++) {
            dest[i] = raster[i] == null ? (byte) 0 : raster[i];
        }
    }

    public void clear(byte r, byte g, byte b) {
        for (int i = 0; i < raster.length; i += 3) {
            raster[i] = b;
            raster[i + 1] = g;
            raster[i + 2] = r;
        }
    }

    void set(int x, int y, Byte r, Byte g, Byte b) {
        if (x < 0 || x >= width || y < 0 || y >= height)
            return;
        int index = (x + y * width) * 3;
        raster[index] = b;
        raster[index + 1] = g;
        raster[index + 2] = r;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }
}

package com.rendering;

import com.math.Vector3f;
import com.model.Vertex;

public class TexturedModelRenderer extends ModelRenderer {

    public TexturedModelRenderer(Display display) {
        super(display);
    }

    @Override
    protected void renderScanBuffer(int min, int max) {
        if (currentModel.isTextured()) {
            super.renderScanBuffer(min, max);
            return;
        }

        for (int y = min; y < max; y++) {
            int xStart = (int) Math.ceil(scanBuffer[y * 2][0]);
            int xEnd = (int) Math.ceil(scanBuffer[y * 2 + 1][0]);

            float difx = scanBuffer[y * 2 + 1][0] - scanBuffer[y * 2][0];

            float zStep = (scanBuffer[y * 2 + 1][1] - scanBuffer[y * 2][1]) / difx;
            float z = scanBuffer[y * 2][1] + zStep * (xStart - scanBuffer[y * 2][0]);

            if (xStart < 0) {
                z += zStep * -xStart;
                xStart = 0;
            }

            if (xEnd > width)
                xEnd = width;

            for (int x = xStart; x < xEnd; x++) {
                float oneOverZ = 1 / z;
                if (display.getZBuffer(x, y) < oneOverZ) {
                    z += zStep;
                    continue;
                }
                display.setZBuffer(x, y, oneOverZ);

                byte lightness = (byte) (((display.getFar() - oneOverZ) / display.getFar()) * 255);

                buffer.set(x, y, lightness, lightness, lightness);

                z += zStep;
            }
        }
    }

    @Override
    protected void scanLineToBuffer(Vertex av, Vertex bv, int handedness) {
        if (currentModel.isTextured()) {
            super.scanLineToBuffer(av, bv, handedness);
            return;
        }

        Vector3f a = av.getPosition();
        Vector3f b = bv.getPosition();

        int miny = (int) Math.ceil(a.getY());
        int maxy = (int) Math.ceil(b.getY());

        float dify = 1 / (b.getY() - a.getY());
        float overflow = miny - a.getY();

        float oneOverAZ = 1 / a.getZ();
        float oneOverBZ = 1 / b.getZ();

        float xStep = (b.getX() - a.getX()) * dify;
        float x = a.getX() + xStep * overflow;

        float zStep = (1 * oneOverBZ - 1 * oneOverAZ) * dify;
        float z = oneOverAZ + zStep * overflow;

        if (miny < 0) {
            x += xStep * -miny;
            z += zStep * -miny;
            miny = 0;
        }
        if (maxy > height)
            maxy = height;

        for (int y = miny; y < maxy; y++) {
            scanBuffer[y * 2 + handedness][0] = x;
            scanBuffer[y * 2 + handedness][1] = z;

            x += xStep;
            z += zStep;
        }
    }

    @Override
    protected Vertex getIntersection(Vertex a, Vertex b, float near) {
        if (currentModel.isTextured()) {
            return super.getIntersection(a, b, near);
        }
        float ax = a.getPosition().getX();
        float ay = a.getPosition().getY();
        float az = a.getPosition().getZ();

        float bx = b.getPosition().getX();
        float by = b.getPosition().getY();
        float bz = b.getPosition().getZ();

        float dx = bx - ax;
        float dy = by - ay;

        float change = (near - az) / (bz - az);

        return new Vertex(
                new Vector3f(
                        ax + dx * change,
                        ay + dy * change,
                        near
                ),
                new Vector3f(
                        0,
                        0,
                        0
                )
        );
    }
}

package com.model;

import com.math.Vector3f;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelLoader {

    public static Model load(String modelName) {
        String filename = String.format("obj/%s.obj", modelName);
        String texturePath = String.format("obj/%s.png", modelName);

        String[] lines = loadLinesFromFile(filename);

        Vector3f[] v = getVectorsFromLines(lines, "v ");
        Vector3f[] t = getVectorsFromLines(lines, "vt");

        BufferedImage texture = null;

        if (t != null) {
            try {
                texture = ImageIO.read(new File(texturePath));
            } catch (IOException e) {
            }
        }

        Face[] faces = getFacesFromLines(lines);

        return new Model(v, t, faces, texture);
    }

    private static Face[] getFacesFromLines(String[] lines) {
        ArrayList<Face> associations = new ArrayList<>();
        for (String line : lines) {
            if (line.startsWith("f")) {
                int[] v = new int[3];
                int[] t = new int[3];

                String[] lineSplit = line.split(" ");

                for (int i = 1; i < 4; i++) {
                    String info = lineSplit[i];

                    String[] infoSplit = info.split("/");
                    try {
                        v[i - 1] = Integer.parseUnsignedInt(infoSplit[0]) - 1;
                    } catch (NumberFormatException e) {
                    }

                    if (infoSplit.length > 1) {
                        try {
                            t[i - 1] = Integer.parseUnsignedInt(infoSplit[1]) - 1;
                        } catch (NumberFormatException e) {
                        }
                    }
                }

                associations.add(new Face(v, t));
            }
        }
        return associations.toArray(new Face[associations.size()]);
    }

    private static Vector3f[] getVectorsFromLines(String[] lines, String startsWith) {
        ArrayList<Vector3f> vectors = new ArrayList<>();
        for (String line : lines) {
            if (line.startsWith(startsWith)) {
                String[] lineSplit = line.split(" ");
                vectors.add(new Vector3f(
                        Float.parseFloat(lineSplit[1]),
                        Float.parseFloat(lineSplit[2]),
                        lineSplit.length < 4 ? 0 : Float.parseFloat(lineSplit[3])
                ));
            }
        }
        return vectors.size() > 0 ? vectors.toArray(new Vector3f[vectors.size()]) : null;
    }

    private static String[] loadLinesFromFile(String filename) {
        List<String> lines = new ArrayList<>();
        String str;
        try {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            while ((str = in.readLine()) != null) {
                lines.add(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines.toArray(new String[lines.size()]);
    }
}

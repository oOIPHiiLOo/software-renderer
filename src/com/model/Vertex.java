package com.model;

import com.math.Vector3f;

public class Vertex {

    private Vector3f p;
    private Vector3f t;

    public Vertex(Vector3f position, Vector3f textureCoordinates) {
        p = position;
        t = textureCoordinates;
    }

    public Vector3f getPosition() {
        return p;
    }

    public Vector3f getTextureCoordinate() {
        return t;
    }
}

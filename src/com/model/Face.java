package com.model;

public class Face {

    private int[] positions;
    private int[] textureCoordinates;

    public Face(int[] positions, int[] textureCoordinates) {
        this.positions = positions;
        this.textureCoordinates = textureCoordinates;
    }

    public int[] getPositions() {
        return positions;
    }

    public int[] getTextureCoordinates() {
        return textureCoordinates;
    }
}

package com.model;

import com.math.Transform;
import com.math.Vector3f;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class Model {

    private Vector3f[] p;
    private Vector3f[] t;
    private Face[] faces;

    private int textureWidth;
    private int textureHeight;

    private byte[] texture = null;
    private boolean textured;

    private Transform transform = new Transform();

    Model(Vector3f[] verticies, Vector3f[] textureCoordinates, Face[] faces, BufferedImage texture) {
        this.faces = faces;
        p = verticies;
        t = textureCoordinates;

        textured = t != null && texture != null;

        if (textured) {
            BufferedImage newImage = new BufferedImage(texture.getWidth(), texture.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
            newImage.getGraphics().drawImage(texture, 0, 0, null);
            this.texture = ((DataBufferByte) newImage.getRaster().getDataBuffer()).getData();
            textureWidth = newImage.getWidth();
            textureHeight = newImage.getHeight();
        }
    }

    public Transform getTransform() {
        return transform;
    }

    public void transform(Transform t) {
        transform.apply(t);
    }

    public Vector3f[] getPositions() {
        return p;
    }

    public Vector3f[] getTextureCoordinates() {
        return t;
    }

    public Face[] getFaces() {
        return faces;
    }

    public boolean isTextured() {
        return textured;
    }

    public void getPixelAt(float x, float y, byte[] colour) {
        int xCoord = (short) ((textureWidth - 1) * x);
        int yCoord = (short) ((textureHeight - 1) * y);

        int index = (yCoord * textureWidth + xCoord) * 4;

        index = (index % texture.length + texture.length) % texture.length;

        colour[1] = texture[index + 3];
        colour[2] = texture[index + 2];
        colour[3] = texture[index + 1];
    }
}
